"use strict";
exports.__esModule = true;
function sum(a, b) {
    console.log(a + b);
}
function percent(value, perc) {
    console.log(value * perc / 100);
}
exports["default"] = {
    sum: sum,
    percent: percent
};
