// License ISC
// ₢ 2019 Matheus Luna
// Trainning Typescript && Node.js

// import Class
import Employeer from './model/Employeer'
import Student from './model/Student'
import User from './model/User'

// Constructor Person
interface Person{
    firstName: string
    lastName: string
}

// [Function] return {}
function greeter(person: Person) {
    console.log(`
    Nome: ${person.firstName} \n 
    Sobrenome: ${person.lastName} \n `
)}

// Model User Interface
let employeer = new Employeer(User.firstName, User.lastName)
let student = new Student(User.firstName, User.lastName)

// Running Function greet
greeter(student)
