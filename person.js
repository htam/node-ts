"use strict";
// License ISC
// ₢ 2019 Matheus Luna
// Trainning Typescript && Node.js
exports.__esModule = true;
// import Class
var Employeer_1 = require("./model/Employeer");
var Student_1 = require("./model/Student");
var User_1 = require("./model/User");
// [Function] return {}
function greeter(person) {
    console.log("\n    Nome: " + person.firstName + " \n \n    Sobrenome: " + person.lastName + " \n ");
}
// Model User Interface
var employeer = new Employeer_1["default"](User_1["default"].firstName, User_1["default"].lastName);
var student = new Student_1["default"](User_1["default"].firstName, User_1["default"].lastName);
// Running Function greet
greeter(student);
