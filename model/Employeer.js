"use strict";
exports.__esModule = true;
// Constructor Employeer
var Employeer = /** @class */ (function () {
    function Employeer(
    //private __id: number,
    firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
    }
    return Employeer;
}());
exports["default"] = Employeer;
