"use strict";
exports.__esModule = true;
// Creating Student Class
var Student = /** @class */ (function () {
    function Student(
    //private _id: number,
    firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
    }
    return Student;
}());
exports["default"] = Student;
